const Discord = require('discord.js');
const client = new Discord.Client();
const config = require("./cofing.json");



client.on("ready", () => {
    console.log(`Bot foi iniciado, com ${client.users.size} usuários, em ${client.channels.size} canais, em ${client.guilds.size} servidores.`);
    client.user.setActivity(`Eu estou ${client.guilds.size} servidores`); 
});

client.on('raw', async dados => {
    if(dados.t !== "MESSAGE_REACTION_ADD" && dados.t !== "MESSAGE_REACTION_REMOVE") return 
    if(dados.d.message_id != "654030533449351178") return
    
    let servidor = client.guilds.get("523104384952041482")
    let membro = servidor.members.get(dados.d.user.id)
    
    let topo = servidor.roles.get('653984860465004545'),
        

        if(dados.t === "MESSAGE.REACTION.ADD"){
            if(dados.d.emoji.id === '653394494124982284'){
            if(membro.roles.has(topo)) return
                membro.addRole(topo) 
            
            }
        }
        if(dados.t === "MESSAGE.REACTION.REMOVE"){
            if(dados.d.emoji.id === '653394494124982284'){
            if(membro.roles.has(topo)) return
                membro.removeRole(topo) 
            
            }
        }
                 
        
})


client.on("guildCreate", guild => {
    db.set(guild.id, []).write()
    console.log(`O bot entrou nos servidores: ${guild.name} (id: ${guild.id}). População: ${guild.memberCount} membros!`);
    client.user.setActivity(`Estou em ${client.guilds.size} servidores`);
});

client.on("guildDelete", guild => {
    console.log(`O bot foi removido do servidor: ${guild.name} (id: ${guild.id})`);
    client.user.setActivity(`Serving ${client.guilds.size} servers`);
});

client.on("message", async message => {
    
    if(message.author.bot) return;
    if(message.channel.type === "dm") return;


    const args = message.content.slice(config.prefix.length).trim().split(/ +/);
    const comando = args.shift().toLowerCase();


    if(comando === "ping") {
        const m = await message.channel.send("Ping?");
        m.edit(`Pong! A latência é ${m.createdTimestamp - message.createdTimestamp}ms. A latência da API é ${Math.round(client.ping)}ms`);
}
    
        

});

client.login(config.token);
    
    